﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Roles
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string RoleType { get; set; }
        public bool IsReadAccess { get; set; }
        public bool IsWriteAccess { get; set; }
        public bool IsEditAccess { get; set; }
        public bool IsDeleteAccess { get; set; }
        public bool HasCategoryCreatePower { get; set; }
        public bool HasCategoryDeletePower { get; set; }
        public bool HasCategoryMovePower { get; set; }
        public bool HasCategoryModifyPower { get; set; }
        public bool HasArchivalPower { get; set; }
        public bool CanPromoteOthers { get; set; }
    }

}
