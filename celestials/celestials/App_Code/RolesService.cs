﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using DAL;
/// <summary>
/// Summary description for RolesService
/// </summary>
[WebService]
public class RolesService : System.Web.Services.WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetRoles()
    {
        Roles roles = new Roles();
        JavaScriptSerializer js = new JavaScriptSerializer();

        return js.Serialize(roles);
    }

}
