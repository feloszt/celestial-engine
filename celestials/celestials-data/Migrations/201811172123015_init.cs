namespace celestials_data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        RoleType = c.String(),
                        IsReadAccess = c.Boolean(nullable: false),
                        IsWriteAccess = c.Boolean(nullable: false),
                        IsEditAccess = c.Boolean(nullable: false),
                        IsDeleteAccess = c.Boolean(nullable: false),
                        HasCategoryCreatePower = c.Boolean(nullable: false),
                        HasCategoryDeletePower = c.Boolean(nullable: false),
                        HasCategoryMovePower = c.Boolean(nullable: false),
                        HasCategoryModifyPower = c.Boolean(nullable: false),
                        HasArchivalPower = c.Boolean(nullable: false),
                        CanPromoteOthers = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Roles");
        }
    }
}
