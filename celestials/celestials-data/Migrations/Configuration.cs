namespace celestials_data.Migrations
{
    using celestials_data.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<celestials_data.Models.celestials_dataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(celestials_data.Models.celestials_dataContext context)
        {
            context.Roles.AddOrUpdate(r => r.Name,
                    new Role
                    {
//                        Id = 1,
                        Name = "Celestials",
                        Description = "Founders",
                        RoleType = "Primary",
                        IsReadAccess = true,
                        IsDeleteAccess = true,
                        IsWriteAccess = true,
                        IsEditAccess = true,
                        HasArchivalPower = true,
                        HasCategoryCreatePower = true,
                        HasCategoryDeletePower = true,
                        HasCategoryModifyPower = true,
                        HasCategoryMovePower = true,
                        CanPromoteOthers = true
                    },
                    new Role
                    {
//                        Id = 2,
                        Name = "Authors",
                        Description = "Players",
                        RoleType = "Primary",
                        IsReadAccess = true,
                        IsDeleteAccess = true,
                        IsWriteAccess = true,
                        IsEditAccess = true,
                        HasArchivalPower = false,
                        HasCategoryCreatePower = false,
                        HasCategoryDeletePower = false,
                        HasCategoryModifyPower = false,
                        HasCategoryMovePower = false,
                        CanPromoteOthers = false
                    },
                    new Role
                    {
                        //Id = 3,
                        Name = "Banned",
                        Description = "Banned",
                        RoleType = "Primary",
                        IsReadAccess = false,
                        IsDeleteAccess = false,
                        IsWriteAccess = false,
                        IsEditAccess = false,
                        HasArchivalPower = false,
                        HasCategoryCreatePower = false,
                        HasCategoryDeletePower = false,
                        HasCategoryModifyPower = false,
                        HasCategoryMovePower = false,
                        CanPromoteOthers = false
                    });
        }
    }
}
