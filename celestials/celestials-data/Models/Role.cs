﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace celestials_data.Models
{
    public class Role
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string RoleType { get; set; }
        public bool IsReadAccess { get; set; }
        public bool IsWriteAccess { get; set; }
        public bool IsEditAccess { get; set; }
        public bool IsDeleteAccess { get; set; }
        public bool HasCategoryCreatePower { get; set; }
        public bool HasCategoryDeletePower { get; set; }
        public bool HasCategoryMovePower { get; set; }
        public bool HasCategoryModifyPower { get; set; }
        public bool HasArchivalPower { get; set; }
        public bool CanPromoteOthers { get; set; }
    }
}