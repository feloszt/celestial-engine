namespace celestial_api.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;


    internal sealed class Configuration : DbMigrationsConfiguration<celestial_api.Models.RoleContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(celestial_api.Models.RoleContext context)
        {
        context.Roles.AddOrUpdate(r =>r.Id,
                new Role { Id=1,Name="Celestials", Description="Founders",RoleType="Primary",
                    IsReadAccess =true,IsDeleteAccess=true,IsWriteAccess=true,IsEditAccess=true,
                    HasArchivalPower =true, HasCategoryCreatePower=true,HasCategoryDeletePower=true,
                    HasCategoryModifyPower =true,HasCategoryMovePower=true,CanPromoteOthers=true},
                new Role { Id = 2, Name = "Authors", Description = "Players", RoleType = "Primary",
                    IsReadAccess = true, IsDeleteAccess = true, IsWriteAccess = true, IsEditAccess = true,
                    HasArchivalPower = false, HasCategoryCreatePower = false, HasCategoryDeletePower = false,
                    HasCategoryModifyPower = false, HasCategoryMovePower = false, CanPromoteOthers = false},
                new Role { Id = 3, Name = "Banned", Description = "Banned", RoleType = "Primary",
                    IsReadAccess = false, IsDeleteAccess = false, IsWriteAccess = false, IsEditAccess = false,
                    HasArchivalPower = false, HasCategoryCreatePower = false, HasCategoryDeletePower = false,
                    HasCategoryModifyPower = false, HasCategoryMovePower = false, CanPromoteOthers = false});
        }
    }
}
